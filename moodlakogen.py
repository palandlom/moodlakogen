import csv
from transliterate import translit, get_available_language_codes
import re

# ==============================
# === Files ====================
# ==============================
in_file_path= "/home/user/wshare/moodlakoge/res/cfdb7-2020-03-11.csv"
out_file_path="/home/user/wshare/moodlakoge/res/out.csv"


class UserRecord(object):
    """Represent user info to read and to write"""

    def __init__(self):
        self.user_name = ""
        self.name = ""
        self.surname = ""
        self.middle_name = ""
        self.email = ""
        self.cohort=""

    @staticmethod
    def get_CSV_user_header(delemiter=','):
        # return "username,firstname,lastname,email,course1,group1,cohort1".split(delemiter)
        return "username,firstname,lastname,email,cohort1".split(delemiter)

    def get_CSV_row(self, delemiter=','):
        # return [user.user_name, user.name, user.surname, user.email]
        return [user.user_name, user.name, user.surname, user.email, user.cohort]

    @staticmethod
    def generate_user_name(surname, email):
        regex = re.compile('[^a-zA-Z]')
        xx= translit(user.surname, 'ru', reversed=True)
        username = regex.sub('', translit(user.surname, 'ru', reversed=True))
        return "{}{}".format(username,str(hash(user.email))[1:4])

# =============================================
# === Generation procedure ====================
# =============================================

# === Read users ===
with open(in_file_path) as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    next(readCSV) # skip header
    users=[]
    for row in readCSV:
        print(row)
        print(row[4], row[8])
        user = UserRecord()
        user.email=row[8]
        user.name, user.middle_name, user.surname = row[4].split(' ')
        user.user_name = UserRecord.generate_user_name(user.surname, user.email)
        user.cohort = "prof-orient"
        users.append(user)

# === Write users ===
with open(out_file_path, mode='w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(UserRecord.get_CSV_user_header())
    for user in users:
        writer.writerow(user.get_CSV_row(delemiter=","))

